#!/bin/bash

DEFAULT_LENGTH=30

numlines="${1:-$DEFAULT_LENGTH}"

grep --no-filename -E -v 'Closing connection with <unknown>|salade' \
        /var/log/tinc.testvpn.log.1 \
        /var/log/tinc.testvpn.log \
    | grep -E 'Connection with.*activated$|Closing connection|had unknown identity' \
    | sed -e 's/Peer 127.0.0.1 port .* had unknown/Unknown/' \
    | sed -e 's/tinc.testvpn\[.*\]: //;s/ (127.0.0.1 port .....)//' \
    | tail -n "$numlines"
