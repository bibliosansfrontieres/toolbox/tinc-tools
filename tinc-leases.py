#!/usr/bin/env python3

from isc_dhcp_leases import Lease, IscDhcpLeases

LEASE_FILE = "/var/lib/dhcp/dhcpd.leases"
IGNORED_HOSTNAMES = ["flow", "salade", "vpn-guy-rpi1"]

leases = IscDhcpLeases(LEASE_FILE)
current_leases = leases.get_current()

for lease in current_leases.values():
    if lease.hostname in IGNORED_HOSTNAMES:
        continue
    print(lease.ethernet, lease.ip, lease.hostname)
